#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <time.h>
#include <string.h>
#include <limits.h>
struct SLNode
{
    int data;
    struct SLNode *next;
};

struct DLNode
{
    int data;
    struct DLNode *prev;
    struct DLNode *next;
};
struct SLNode *createSLNode(int data)
{
    struct SLNode *node = malloc(sizeof(struct SLNode));
    node->data = data;
    node->next = NULL;
    return node;
}
struct DLNode *createDLNode(int data)
{
    struct DLNode *node = malloc(sizeof(struct DLNode));
    node->data = data;
    node->prev = NULL;
    node->next = NULL;
    return node;
}
void dlnode_print(struct DLNode *node)
{
    int counter = 0;
    if (node != NULL)
    {
        while (node != NULL)
        {
            printf("%i ", node->data);
            node = node->next;
            counter++;
        }
        printf("\nlength of the dual linked list: %i\n", counter);
    }
    else
    {
        printf("Dual linked list is empty\n");
    }
}
void slnode_print(struct SLNode *node)
{
    int counter = 0;
    if (node != NULL)
    {
        while (node != NULL)
        {
            printf("%i ", node->data);
            counter++;
            node = node->next;
        }
        printf("\nlength of the single linked list: %i\n", counter);
    }
    else
    {
        printf("Single linked list is empty\n");
    }
}
struct DLNode *addDLNode(struct DLNode *head, int num)
{
    struct DLNode *new_node = createDLNode(num);
    struct DLNode *tmp_node;
    struct DLNode *min_node;
    struct DLNode *tmp;
    if (head == NULL)
    {
        head = new_node;
        head->data = new_node->data;
        head->next = NULL;
        head->prev = NULL;
        return head;
    }
    else if (head->next == NULL)
    {
        tmp_node = head->next;
        new_node->next = head;
        new_node->prev = NULL;
        head->prev = new_node;
        return new_node;
    }
    else
    {
        int min = INT_MAX;
        tmp_node = head;
        min_node = head;
        while (tmp_node != NULL)
        {
            if (tmp_node->data < min)
            {
                min_node = tmp_node;
                min = tmp_node->data;
            }
            tmp_node = tmp_node->next;
        }
        if (min_node == head)
        {
            new_node->prev = NULL;
            new_node->next = head;
            head->prev = new_node;
            return new_node;
        }
        else
        {
            tmp = head;
            while (tmp->next != min_node)
            {
                tmp = tmp->next;
            }
            new_node->prev = tmp;
            new_node->next = tmp->next;
            tmp->next = new_node;
            new_node->next->prev = new_node;
            return head;
        }
    }
}
void DLnode_clear(struct DLNode *head)
{
    struct DLNode *node = head;
    while (node != NULL)
    {
        struct DLNode *next = node->next;
        free(node);
        node = next;
    }
}
void SLnode_clear(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        struct SLNode *next = node->next;
        free(node);
        node = next;
    }
}
struct SLNode *addSLNode(struct SLNode *head, int data)
{
    struct SLNode *new_node = createSLNode(data);
    if (head == NULL)
    {
        head = new_node;
        return head;
    }
    else if (head->next == NULL)
    {
        new_node->next = head;
        return new_node;
    }
    else
    {
        struct SLNode *tmp_node = head;
        while (tmp_node->next->next != NULL)
        {
            tmp_node = tmp_node->next;
        }
        new_node->next = tmp_node->next;
        tmp_node->next = new_node;
        return head;
    }
}
struct DLNode *removeDLnode(struct DLNode *head)
{
    if (head == NULL)
    {
        return head;
    }
    struct DLNode *tmp_node;
    tmp_node = head;
    while (tmp_node != NULL)
    {
        if (tmp_node->data > 0)
        {
            if (tmp_node->prev == NULL)
            {
                struct DLNode *temp = tmp_node;
                tmp_node = tmp_node->next;
                free(temp);
                if (tmp_node != NULL)
                {
                    tmp_node->prev = NULL;
                }
                head = tmp_node;
            }
            else
            {
                struct DLNode *next = tmp_node->prev;
                struct DLNode *tmp = tmp_node;
                tmp_node = tmp_node->next;
                free(tmp);
                next->next = tmp_node;
                if (tmp_node != NULL)
                {
                    tmp_node->prev = next;
                }
            }
        }
        else
        {
            tmp_node = tmp_node->next;
        }
    }
    return head;
}

struct SLNode *createSecondList(struct DLNode *head)
{
    if (head != NULL)
    {
        struct DLNode *tmp_node;
        tmp_node = head;
        struct SLNode *sl_head = NULL;
        while (tmp_node != NULL)
        {
            if (tmp_node->data > 0)
            {
                sl_head = addSLNode(sl_head, tmp_node->data);
            }
            tmp_node = tmp_node->next;
        }
        return sl_head;
    }
    else
    {
        return NULL;
    }
}
int main()
{
    int number = 0;
    int dual_num = 0;
    struct DLNode *head = NULL;
    struct SLNode *sl_head = NULL;
    struct SLNode *tsl_head = NULL;
    do
    {
        printf("Enter:\n1 to add a number to a dual linked list\n2 to create a single linked list\n3 to exit\n");
        scanf("%i", &number);
        if (number > 0 && number < 4)
        {
            if (number == 1)
            {
                printf("\nEnter the number to add to dual linked list: ");
                scanf("%i", &dual_num);
                head = addDLNode(head, dual_num);
                printf("\nDual linked list :\n ");
                dlnode_print(head);
                printf("\nSingle linked list :\n ");
                slnode_print(sl_head);
            }
            else if (number == 2)
            {
                if (head != NULL)
                {
                    tsl_head = createSecondList(head);
                    if (tsl_head == NULL)
                    {
                        printf("\nThere are no positive numbers in dual linked list\n");
                        printf("\nDual linked list : ");
                        dlnode_print(head);
                    }
                    else
                    {
                        sl_head = tsl_head;
                        head = removeDLnode(head);
                        printf("\nSingle linked list : ");
                        slnode_print(sl_head);
                        printf("\nDual linked list : ");
                        dlnode_print(head);
                    }
                }
                else
                {
                    printf("\nSingle linked list cannot be created\n");
                    printf("\nDual linked list : ");
                    dlnode_print(head);
                }
            }
            else
            {
                DLnode_clear(head);
                SLnode_clear(sl_head);
                break;
            }
        }
        else
        {
            printf("\n Number must be 1, 2 or 3\n\n");
        }
    } while (number != 3);
}
