#include <stdio.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define KRED "\x1B[31m"
#define RESET "\x1B[0m"
int main()
{
    srand(time(0));
    int N = 0, M = 0, K = 0, f = 0;
    printf("enter size(N and M) and K\n");
    scanf("%i %i %i", &N, &M, &K);
    if (M <= 0 || N <= 0 || K <= 0)
    {
        printf("incorrect size\n");
        return 0;
    }
    else
    {
        int array[N][M];
        bool same;
        for (int j = 0; j < M; j++)
        {
            for (int i = 0; i < N;)
            {
                same = false;
                int arr = rand() % (99 - 10 + 1) + 10;
                for (int a = 0; a < M; a++)
                {
                    for (int b = 0; b < N; b++)
                    {
                        if (arr == array[a][b])
                        {
                            same = true;
                        }
                    }
                }
                if (same == false)
                {
                    array[i][j] = arr;
                    i++;
                }
            }
        }
        if (K % 2 == 0)
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    printf(KRED "%d " RESET, array[i][j]);
                }
                printf("\n");
            }
            printf("\n");
            for (int i = 0; i < N; i++)
            {
                int gap = M;
                bool sorted = false;
                while (sorted = false || gap > 1)
                {
                    gap = gap / 1.247;
                    sorted = true;
                    for (int j = 0; j < M - gap; j++)
                    {
                        if (array[i][j] > array[i][j + gap])
                        {
                            int buffer = array[i][j];
                            array[i][j] = array[i][j + gap];
                            array[i][j + gap] = buffer;
                            sorted = false;
                        }
                    }
                }
            }
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    printf(KRED "%d " RESET, array[i][j]);
                }
                printf("\n");
            }
        }
        else
        {
            float p = M / 2.0;
            int m=0;
            if(abs(p)!=fabs(p))
            {
                m=abs(p)+1;
            }
            else
            {
                m=abs(p);
            }
            int sort[N][m];
            for (int i = 0; i < N; i++)
            {
                f = 0;
                for (int j = 0; j < M; j++)
                {
                    if (j % 2 == 0)
                    {
                        printf(KRED "%d " RESET, array[i][j]);
                        sort[i][f] = array[i][j];
                        f++;
                    }
                    else
                    {
                        printf("%d ", array[i][j]);
                    }
                }
                printf("\n");
            }
            printf("\n");
            for (int i = 0; i < N; i++)
            {
                int gap = m;
                bool sorted = false;
                while (sorted = false || gap > 1)
                {
                    gap = gap / 1.247;
                    sorted = true;
                    for (int j = 0; j < m - gap; j++)
                    {
                        if (sort[i][j] > sort[i][j + gap])
                        {
                            int buffer = sort[i][j];
                            sort[i][j] = sort[i][j + gap];
                            sort[i][j + gap] = buffer;
                            sorted = false;
                        }
                    }
                }
            }
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    int s=j*2;
                    array[i][s] = sort[i][j];
                }
            }
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    if (j % 2 == 0)
                    {
                        printf(KRED "%d " RESET, array[i][j]);
                    }
                    else
                    {
                        printf("%d ", array[i][j]);
                    }
                }
                printf("\n");
            }
            printf("\n");
        }
    }
}