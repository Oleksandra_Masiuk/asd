#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
int main()
{
    srand(time(0));
    int a=0, b = 0, n, m, min, max = 0, x1, y1, x2, y2, num, i, j, M, N, c = 0;
    printf(" enter number of lines and columns");
    scanf("%d", &n);
    scanf("%d", &m);
    int perev[n][m];
    int D = (m * n);
    M = m;
    N = n;
    
    if (n <= 0 || m <= 0)
    {
        printf("incorrect N or M");
    }
    else
    {
        printf("Press 1 to fill the matrix randomly or 2 to fill the matrix by numbers from 0 to N * M\n");
        scanf("%i", &num);
        if ((num != 1) && (num != 2))
        {
            printf("incorrect number\n");
        }
        else
        {
            if (num == 1)
            {
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < m; j++)
                    {
                        perev[i][j] = rand() % (99 - 10 + 1) + 10;
                        printf(" %2d", perev[i][j]);
                    }
                    printf("\n");
                }
            }
            if (num == 2)
            {
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < m; j++)
                    {
                        perev[i][j] = m * i + j;
                        printf(" %2d", perev[i][j]);
                    }
                    printf("\n");
                }
            }
            printf("\n");
            while (c < D)
            {
                for (j = m - 1; j >= b + 1; j--)
                {
                    if (a < D)
                    {
                        printf(" %2d", perev[n - 1][j]);
                        a = a + 1;
                    }
                }
                for (i = n - 1; i >= b; i--)
                {
                    if (a < D)
                    {
                        printf(" %2d", perev[i][b]);
                        a = a + 1;
                    }
                }
                for (j = b + 1; j < m; j++)
                {
                    if (a < D)
                    {
                        printf(" %2d", perev[b][j]);
                        a = a + 1;
                    }
                }
                for (i = b + 1; i <= n - 2; i++)
                {
                    if (a < D)
                    {
                        printf(" %2d", perev[i][m - 1]);
                        a = a + 1;
                    }
                }
                m = m - 1;
                n = n - 1;
                b = b + 1;
                c=c+1;
            }
            printf("\n");
            min = 10000;
            for (i = 0; i < N; i++)
            {
                for (j = 0; j < M; j++)
                {
                    if (((i < j) && (i + j < M - 1)) || ((i > j) && (i + j > M - 1)))
                    {
                        if (min > perev[i][j])
                        {
                            min = perev[i][j];
                            x1 = j;
                            y1 = i;
                        }
                    }
                }
            }
            if (min != 10000)
            {
                printf(" [%i][%i] min=%i\n", y1, x1, min);
            }
            else
            {
                printf("there isn`t min\n");
            }
            for (i = 0; i < N; i++)
            {
                for (j = 0; j < M; j++)
                {
                    if ((i > j) || (i + j > M - 1) || ((i > j) && (i + j > M - 1)))
                    {
                        if (max < perev[i][j])
                        {
                            max = perev[i][j];
                            x2 = j;
                            y2 = i;
                        }
                    }
                }
            }
            if (max != 0)
            {
                printf(" [%i][%i] max=%i\n", y2, x2, max);
            }
            else
            {
                printf("there isn`t max\n");
            }
        }
    }
}
