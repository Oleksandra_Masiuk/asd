#include <stdio.h>
#include <math.h>
int main()
{
    double x, y, z, a, b;
    scanf("%lf %lf %lf", &x, &y, &z);
    if (x == 2 || x == -2 || pow(z, -x - 2) == 1 / (4 - pow(x, 2)) || (z < 0 && fabs(-x - 2) < 1) || (z == 0 && (-x - 2) < 0))
    {
        printf("ERROR\n");
    }
    else
    {
        a = (1 + y) * (x + (y/(pow(x, 2) + 4)))/((pow(z, (-x-2))+(1/(pow(x, 2)- 4))));
        if ((pow(x, 4) / 2) == pow(sin(z), 2) || (cos(a - 2) < 0 && fabs(y) < 1) || (cos(a - 2) == 0 && y < 0))
        {
            printf("a=%lf\n", a);
            printf("b cannot be calculated ");
        }
        else
        {
            b = (1 + pow(cos(a - 2), y)) / ((pow(x, 4) / 2) - pow(sin(z), 2));
            printf("a=%lf\nb=%lf", a, b);
        }
    }
    return 0;
}
