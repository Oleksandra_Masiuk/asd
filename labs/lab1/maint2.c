#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int vys(int y)
{
    int sd = 0;
    if (y % 100 == 0)
    {
        if (y % 400 == 0)
        {
            sd = 28; //nev
        }
        else
        {
            sd = 29; //vys
        }
    }
    else
    {
        if (y % 4 == 0)
        {
            sd = 29;
        }
        else
        {
            sd = 28;
        }
    }
    return sd;
}
int kdniv(int m, int y)
{
    int sd = 0;
    if (m < 8 && m != 2)
    {
        if (m % 2 == 0)
        {
            sd = 30;
        }
        else
        {
            sd = 31;
        }
    }
    else if (m > 7)
    {
        if (m % 2 == 0)
        {
            sd = 31;
        }
        else
        {
            sd = 30;
        }
    }
    else if (m == 2)
    {
        sd = vys(y);
    }
    return sd;
}
int main()
{
    int d1 = 0, d2 = 0, m1 = 0, m2 = 0, y1 = 0, y2 = 0, dd = 0, dr = 0, sd1 = 0, sd2 = 0, sd = 0,  s1 = 0, s2 = 0, s = 0;
    scanf("%i %i %i %i %i %i", &d1, &m1, &y1, &d2, &m2, &y2);
    sd1 = kdniv(m1, y1);
    sd2 = kdniv(m2, y2);
    if (d1 > 0 && d2 > 0 && m2 > 0 && m1 > 0 && y1 > 0 && y2 > 0 && d1 <= sd1 && d2 <= sd2)
    {
        for (int i = 1; i < m1; i++)
        {
            sd1 = kdniv(i, y1);
            s1 = s1 + sd1;
        }
        for (int i = 1; i < m2; i++)
        {
            sd2 = kdniv(i, y2);
            s2 = s2 + sd2;
        }
        s1 = s1 + d1;
        s2 = s2 + d2;

        if (y2 >= y1)
        {
            if (s2 >= s1)
            {
                for (m1; m1 < m2; m1++)
                {
                    sd = kdniv(m1, y1);
                    s = s + sd;
                }
                dd = s + d2 - d1;
                dr = y2 - y1;
                printf("dd=%i dr=%i", dd, dr);
            }
            else
            {
                for (m1; m1 < 13; m1++)
                {
                    sd1 = kdniv(m1, y1);
                    s = s + sd1;
                }
                dd = s - d1 + s2;
                dr = y2 - y1 - 1;
                printf("dd=%i dr=%i", dd, dr);
            }
        }
        else
        {
            if (s2 <= s1)
            {
                for (m2; m2 < m1; m2++)
                {
                    sd = kdniv(m2, y2);
                    s = s + sd;
                }
                dd = s - d2 + d1;
                dr = y1 - y2;
                printf("dd=%i dr=%i", dd, dr);
            }
            else
            {
                for (m2; m2 < 13; m2++)
                {
                    sd2 = kdniv(m2, y2);
                    s = s + sd2;
                }
                dd = s - d2 + s1;
                dr = y1 - y2 - 1;
                printf("dd=%i dr=%i", dd, dr);
            }
        }
        
    }
    else
    {
        printf("ERROR\n");
    }
}