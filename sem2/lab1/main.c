#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ctype.h>
#define KRED "\x1B[31m"
#define RESET "\x1B[0m"

void fillarray(int N, int M, int perev[N][M], int num)
{
    if (num == 1)
    {
        bool same;
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M;)
            {
                same = false;
                int arr = rand() % (199 - 0 + 1) + 0;
                for (int a = 0; a < N; a++)
                {
                    for (int b = 0; b < M; b++)
                    {
                        if (arr == perev[a][b])
                        {
                            same = true;
                        }
                    }
                }
                if (same == false)
                {
                    perev[i][j] = arr;
                    j++;
                }
            }
        }
    }
    if (num == 2)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                perev[i][j] = M * i + j;
            }
        }
    }
}

void printarray(int N, int M, int perev[N][M])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if ((i < j) && (i + j < M - 1))
            {
                printf(KRED "%2d " RESET, perev[i][j]);
            }
            else
            {
                printf("%2d ", perev[i][j]);
            }
        }
        printf("\n");
    }
}

void countsort(int k, int size, int buckets[k][size], int *len)
{
    for (int j = 0; j < k; j++)
    {
        if (len[j] > 0)
        {
            int max = INT32_MIN;
            for (int i = 0; i < len[j]; i++)
            {
                if (max < buckets[j][i])
                {
                    max = buckets[j][i];
                }
            }
            int count[max + 1];
            for (int i = 0; i < max + 1; i++)
            {
                count[i] = 0;
            }
            for (int i = 0; i < len[j]; i++)
            {
                count[buckets[j][i]] = count[buckets[j][i]] + 1;
            }
            for (int i = 1; i < max + 1; i++)
            {
                count[i] = count[i] + count[i - 1];
            }
            int sorted[len[j]];
            for (int i = 0; i < len[j]; i++)
            {
                sorted[i] = buckets[j][i];
            }
            for (int i = len[j] - 1; i >= 0; i--)
            {
                if (count[sorted[i]] > 0)
                {
                    count[sorted[i]] = count[sorted[i]] - 1;
                    buckets[j][count[sorted[i]]] = sorted[i];
                }
            }
        }
    }
}

int main()
{
    srand(time(0));
    int a = 0, b = 0, num, i, j, M, N, c = 0;
    printf(" enter number of lines and columns");
    scanf("%d %d", &N, &M);
    int perev[N][M];
    if (N <= 0 || M <= 0)
    {
        printf("incorrect N or M");
    }
    else
    {
        printf("Press 1 to fill the matrix randomly or 2 to fill the matrix by numbers from 0 to N * M\n");
        scanf("%i", &num);
        if ((num != 1) && (num != 2))
        {
            printf("incorrect number\n");
        }
        else
        {
            fillarray(N, M, perev, num);
            printarray(N, M, perev);
        }
        int capacity = M * N;
        int arr[capacity];
        int size = 0;
        for (i = 0; i < N; i++)
        {
            for (j = 0; j < M; j++)
            {
                if ((i < j) && (i + j < M - 1))
                {
                    arr[size] = perev[i][j];
                    size++;
                }
            }
        }
        int max = INT32_MIN;
        for (int i = 0; i < size; i++)
        {
            if (arr[i] > max)
            {
                max = arr[i];
            }
        }
        const int k = (max / 10) + 1;
        int buckets[k][size];
        int len[k];
        for (int i = 0; i < k; i++)
        {
            len[i] = 0;
        }
        for (int i = 0; i < size; i++)
        {
            int a = len[arr[i] / 10];
            buckets[arr[i] / 10][a] = arr[i];
            len[arr[i] / 10]++;
        }
        int l = 0;
        countsort(k, size, buckets, len);
        for (int i = 0; i < k; i++)
        {
            for (int j = 0; j < len[i]; j++)
            {
                arr[l] = buckets[i][j];
                l++;
            }
        }
        int s = 0;
        for (i = 0; i < N; i++)
        {
            if ((i % 2 == 0))
            {
                for (j = 0; j < M; j++)
                {
                    if ((i < j) && (i + j < M - 1))
                    {
                        perev[i][j] = arr[s];
                        s++;
                    }
                }
            }
            else
            {
                for (j = M - 1; j > 0; j--)
                {
                    if ((i < j) && (i + j < M - 1))
                    {
                        perev[i][j] = arr[s];
                        s++;
                    }
                }
            }
        }
        printf("\n");
        printarray(N, M, perev);
    }
}