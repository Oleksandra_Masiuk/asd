#include <iostream>
#include <cmath>
using namespace std;
void printArray(string arr[], int n) 
{ 
    for (int i=0; i<n; ++i) 
    cout << arr[i] << " "; 
    cout << "\n"; 
} 
int strings_number(int n) 
{
    int counter = 0;
    while ((int)pow(2, counter) <= n) 
    {
        counter++;
    }
    return counter;
}
int avg_length(string arr[], int n) 
{
    int result = 0;
    for (int i = 0; i < n; i++) 
    {
        result += arr[i].length() + 2;
    }
    return result / n;
}

void TreePrint(string array[], int n) 
{
    int counter1 = 0;
    int counter2 = 0;
    int counter3 = 1;
    int spaces_between[strings_number(n)];
    spaces_between[0] = avg_length(array, n);
    for (int i = 1; i < strings_number(n); i++) 
    {
        spaces_between[i] = spaces_between[i - 1] + avg_length(array, n) * (int)pow(2, counter3);
        counter3++;
    }
    for (int i = 0; i < strings_number(n); i++) 
    {
        for (int l_spaces = 0; l_spaces < avg_length(array, n) * (int)pow(2, strings_number(n) - 1 - counter2); l_spaces++) 
        {
            cout << " ";
        }
        for (int j = counter1; j < (int)pow(2, counter2 + 1) - 1; j++) 
        {
            if (j >= n) 
            {
                break;
            }
            cout  << array[j];
            for (int spaces = 0; spaces < spaces_between[strings_number(n) - 1 - counter2]; spaces++) 
            {
                cout << " ";
            }
        }
        counter1 = (int)pow(2, counter2 + 1) - 1;
        counter2++;
        cout << endl;
        cout << endl;
    }
}
void heapify(string arr[], int n, int i)
{
    int smallest = i;   // Initialize smallest as root
    int l = 2 * i + 1; // left = 2*i + 1
    int r = 2 * i + 2; // right = 2*i + 2
    // If left child is smaller than root
    if (l < n &&arr[l]< arr[smallest])
    {
        smallest = l;
    }
    // If right child is smaller than smallest so far
    if (r < n &&arr[r] < arr[smallest])
    {
        smallest = r;
    }
    // If smallest is not root
    if (smallest != i)
    {
        string tmp = arr[i];
        arr[i] = arr[smallest];
        arr[smallest] = tmp;
        // Recursively heapify the affected sub-tree
        heapify(arr, n, smallest);
    }
}
void heapSort(string arr[], int n)
{
    cout << "our array: " << endl;
    printArray(arr, n);
    cout << endl;
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        heapify(arr, n, i);
    }
    cout << "rearranged array: " << endl;
    printArray(arr, n);
    cout << endl;
    cout << "tree:" << endl;
    cout << endl;
    TreePrint(arr, n);
    cout << endl;
    // One by one extract an element from heap
    for (int i = n - 1; i > 0; i--)
    {
        string tmp = arr[0];
        arr[0] = arr[i];
        arr[i] = tmp;
        // call min heapify on the reduced heap
        heapify(arr, i-1, 0);
    }
    cout << "sorted array: " << endl;
    printArray(arr, n);
}

int main()
{
    int option = 0;
    std::cout << "'1' - for test, '2' - for own array ";
    string opt;
    getline(cin, opt);
    option=stoi(opt);
    if (option != 1 && option != 2) 
    {
        cout << "Number must be '1' or '2'";
        exit(1);
    }
    if (option == 1) 
    {
        string test[] = {"some", "book", "june", "ours", "tips"};
        heapSort(test, 5);
        exit(1);
    }
    cout << "enter N" << endl;
    string N;
    getline(cin, N);
    string arr[stoi(N)];
    for (int i = 0; i < stoi(N); i++)
    {
        string str;
        getline(cin, str);
        arr[i] = str;
        cout << arr[i] << " " << i << endl;
        cin.clear();
    }
    heapSort(arr , stoi(N));
}