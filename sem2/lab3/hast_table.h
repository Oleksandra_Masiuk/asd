#pragma once
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include "key_value.h"
using namespace std;
class Hashtable
{
    int loadness;
    int size;

public:
    vector<BookPair> table{3};

    Hashtable();
    ~Hashtable();
    void rehashing(); //+
    void insert(BookKey key, BookValue &value);
    bool remove(BookKey key);
    BookPair find_at(BookKey key);
    int hash_code(int key);
    int gethash(int key);
    void print();
    bool exist(int key);
};
bool ifPrimeNumber(int number);
int searchNumber(int number);