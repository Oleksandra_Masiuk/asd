#include <iostream>
#include <string>
#include "hast_table.h"
#include "hashtable2.h"
using namespace std;
int main()
{
    Hashtable t;
    hashtable t2;
    int command = 0;
    cout << endl;
    cout << "Welcome to ebook sysmem" << endl;
    cout << endl;
    while (command != 6)
    {
        cout << "Please, choose one of the options below: " << endl;
        cout << "'1' - To add new book." << endl;
        cout << "'2' - To remove book." << endl;
        cout << "'3' - To find book." << endl;
        cout << "'4' - To show both hashtables." << endl;
        cout << "'5' - To show all books, written by the same author." << endl;
        cout << "'7' - To fill automatically." << endl;
        cout << "'6' - Exit." << endl;
        cin >> command;
        if (command < 1 || command > 7)
        {
            cout << "Error, please enter the number '1', '2', '3', '4', '5', '6' or '7'." << endl;
        }
        else if (command == 1)
        {
            string title;
            int yearofpublishing;
            int number;
            BookKey key;
            cout << "Enter Id:" << endl;
            cin >> key.bookID;
            cout << "Enter title: ";
            cin >> title;
            cout << "Enter year of publishing: ";
            cin >> yearofpublishing;
            cout << "Enter number of authors: ";
            cin >> number;
            AuthorsList l;
            cout << "Enter " << number << " author(s)";
            for (int i = 0; i < number + 1; i++)
            {
                string s;
                getline(cin, s);
                l.AuthorsName.push_back(s);
                AuthorKey k;
                k.AuthorName = s;
                AuthorValue v;
                v.Bookid.push_front(key.bookID);
                t2.insert(k, v);
            }
            BookValue value{title, l, yearofpublishing};
            t.insert(key, value);
        }
        else if (command == 2)
        {
            string title;
            BookKey key;
            cout << "Enter Id:" << endl;
            cin >> key.bookID;
            if (!t.exist(key.bookID))
            {
                cout << "cannot remove" << endl;
            }
            else
            {
                t.remove(key);
                t.print();
            }
        }
        else if (command == 3)
        {
            int key;
            cout << "Enter key,please" << endl;
            cin >> key;
            BookKey k{key};
            if (!t.exist(key))
            {
                cout << "cannot remove" << endl;
            }
            else
            {
                BookPair b = t.find_at(k);
                cout << "Key: " << b.book_key.bookID << endl;
                cout << "Title: " << b.book_value.title << endl;
                cout << "Year of Publishing: " << b.book_value.yearofPublishing << endl;
                cout << "Authors:"
                     << "   ";
                for (auto item = b.book_value.authors.AuthorsName.begin(); item != b.book_value.authors.AuthorsName.end(); item++)
                {
                    cout << (*item) << "  ";
                }
                cout << endl;
            }
        }
        else if (command == 4)
        {
            t.print();
            t2.print();
        }
        else if (command == 5)
        {
            cout << "Enter author name" << endl;
            string name;
            cin >> ws;
            getline(cin, name);
            if (!t2.exist(name))
            {
                cout << "cannot find this author\n";
            }
            else
            {
                AuthorAndIndex v = t2.findAllBooks(name);
                cout << "Name: " << name << endl;
                cout << "BookID: " << endl;
                for (auto item = v.author_value.Bookid.begin(); item != v.author_value.Bookid.end(); item++)
                {
                    cout << (*item) << endl;
                }
            }
        }
        else if (command == 7)
        {

            for (int i = 1; i < 4; i++)
            {
                BookKey key;
                key.bookID = i;
                BookValue v;
                string name;
                if (i == 1)
                {
                    v.title = "Some book";
                    name = "S D";
                    v.yearofPublishing = 2012;
                }
                else if (i == 2)
                {
                    v.title = "My life";
                    name = "S M";
                    v.yearofPublishing = 2002;
                }
                else
                {
                    v.title = "KPI";
                    name = "I S";
                    v.yearofPublishing = 1912;
                }
                v.authors.AuthorsName.push_front(name);
                AuthorKey k{name};
                AuthorValue val;
                val.Bookid.push_front(key.bookID);
                t.insert(key, v);
                t2.insert(k, val);
            }
        }
    }
}