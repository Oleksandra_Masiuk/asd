#pragma once
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include "key_value.h"
using namespace std;
class hashtable
{
    int loadness;
    int size;
public:
    vector<AuthorPair> table{3};
    hashtable();  //+
    ~hashtable(); //+
    void rehashing(); //+
    void insert(AuthorKey & key, AuthorValue &value); //+
    int hash_code(string &Name); //+
    int gethash(string &Name); //+
    void print(); //+
    AuthorAndIndex findAllBooks(string &name);
    bool exist(string Name);
};
bool ifprimenumber (int number);
int searchnumber (int number);