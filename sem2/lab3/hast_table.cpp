#include <iostream>
#include <optional>
#include <math.h>
#include "hast_table.h"
using namespace std;
Hashtable::Hashtable()
{
    size = 3;
    loadness = 0;
    for (int i = 0; i < size; i++)
    {
        table.at(i).book_key.bookID = -1;
    }
}
Hashtable::~Hashtable() {}
int Hashtable::hash_code(int key)
{
    return key;
}
int Hashtable::gethash(int key)
{
    double loadfactor = (double)loadness / (double)size;
    if (loadfactor > 0.5)
    {
        rehashing();
    }
    int code = this->hash_code(key);
    int index = code % 7;
    bool empty = false;
    int i = 0;
    while (!empty)
    {
        index += (i * (code % 5 + 1));
        if (table.at(index % size).book_key.bookID < 0 || table.at(index % size).book_key.bookID == key)
        {
            index %= size;
            empty = true;
        }
        else
        {
            index = code % 7;
        }
        i++;
    }
    return index;
}
bool Hashtable::exist(int key)
{
    int code = hash_code(key);
    int hash = code % 7;
    int i = 0;
    while (table[hash % size].book_key.bookID != -1)
    {
        if (table[hash % size].book_key.bookID == key)
        {
            return true;
        }
        if (i == size)
        {
            break;
        }
        hash += (code % 5 + 1);
        i++;
    }
    return false;
}

void Hashtable::insert(BookKey key, BookValue &value)
{
    int hash = gethash(key.bookID);
    table[hash].book_key.bookID = key.bookID;
    table[hash].book_value = value;
    loadness++;
}
bool Hashtable::remove(BookKey key)
{
    int code = this->hash_code(key.bookID);
    int hash = code % 7;
    while (table[hash % size].book_key.bookID != key.bookID)
    {
        hash += (code % 5 + 1);
    }
    loadness--;
    table[hash % size].book_key.bookID = -2;
}
void Hashtable::print()
{
    cout << endl;
    cout << " Main Hashtable now looks like: " << endl;
    cout << endl;
    for (int i = 0; i < size; i++)
    {
        cout << endl;
            cout << "Slot number " << i << endl;
        if (table.at(i).book_key.bookID > -1)
        {
            cout << "Key: " << table.at(i).book_key.bookID << " | " << "Title: " << table.at(i).book_value.title <<" | " << "Year of Publishing: " << table.at(i).book_value.yearofPublishing << endl;
            cout << "Authors:"
                 << "  ";
            for (auto item = table.at(i).book_value.authors.AuthorsName.begin(); item != table.at(i).book_value.authors.AuthorsName.end(); item++)
            {
                cout << (*item) << " | ";
            }
            cout << endl;
        }
        else
        {
            cout << endl
                 << "Empty" << endl;
        }
    }
}
BookPair Hashtable::find_at(BookKey key)
{
    int code = hash_code(key.bookID);
    int index1 = code % 7;
    int index2 = code % 5 + 1;
    while (table.at((index1) % size).book_key.bookID != key.bookID)
    {
        index1 += index2;
    }
    return table.at(index1 % size);
}
bool ifPrimeNumber(unsigned int number)
{
    float temp = number;
    int counter = 0;
    for (int i = 1; i <= number; i++)
    {
        if ((number - (temp / i)) == (number - (number / i)))
        {
            counter++;
        }
    }
    if (counter == 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

unsigned int searchNumber(unsigned int number)
{
    bool if_prime = ifPrimeNumber(number);
    if (if_prime == false)
    {
        while (ifPrimeNumber(number) != true)
        {
            number++;
        }
    }
    return number;
}
void Hashtable::rehashing()
{
    unsigned int new_size = size;
    new_size++;
    int counter = 1;
    while (new_size / 2 != 1)
    {
        counter++;
        new_size /= 2;
    }
    unsigned int number = pow(2, counter + 1) - 1;
    number = searchNumber(number);
    vector<BookPair> new_table{number};
    cout << "rehashing main table, new size=" << number << endl;
    for (int i = 0; i < size; i++)
    {
        new_table[i] = table[i];
    }
    //
    int old_size = size;
    for (int i = 0; i < size; i++)
    {
        table.pop_back();
    }
    table.resize(number);
    size = number;
    for (int i = 0; i < size; i++)
    {
        table.at(i).book_key.bookID = -1;
    }
    loadness = 0;
    for (int i = 0; i < old_size; i++)
    {
        if (new_table[i].book_key.bookID > -1)
        {
            insert(new_table[i].book_key, new_table[i].book_value);
        }
    }
}