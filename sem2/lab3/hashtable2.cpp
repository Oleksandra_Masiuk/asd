#include <optional>
#include <math.h>
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include "hashtable2.h"
using namespace std;
hashtable::hashtable()
{
    size = 3;
    loadness = 0;
}
hashtable::~hashtable() {}
bool ifprimenumber(unsigned int number)
{
    float temp = number;
    int counter = 0;
    for (int i = 1; i <= number; i++)
    {
        if ((number - (temp / i)) == (number - (number / i)))
        {
            counter++;
        }
    }
    if (counter == 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

unsigned int searchnumber(unsigned int number)
{
    bool if_prime = ifprimenumber(number);
    if (if_prime == false)
    {
        while (ifprimenumber(number) != true)
        {
            number++;
        }
    }
    return number;
}
void hashtable::rehashing()
{
    unsigned int new_size = size;
    new_size++;
    int counter = 1;
    while (new_size / 2 != 1)
    {
        counter++;
        new_size /= 2;
    }
    unsigned int number = pow(2, counter + 1) - 1;
    number = searchnumber(number);
    vector<AuthorPair> new_table{number};
    cout << "rehashing second table, new size=" << number << endl;
    for (int i = 0; i < size; i++)
    {
        new_table[i] = table[i];
    }
    //
    int old_size = size;
    for (int i = 0; i < size; i++)
    {
        table.pop_back();
    }
    table.resize(number);
    size = number;
    loadness = 0;
    for (int i = 0; i < old_size; i++)
    {
        if (new_table[i].author_key.AuthorName != "")
        {
            insert(new_table[i].author_key, new_table[i].author_value);
        }
    }
}
int hashtable::gethash(string &Name)
{
    double loadfactor = (double)loadness / (double)size;
    if (loadfactor > 0.5)
    {
        rehashing();
    }
    int code = hash_code(Name);
    int hash = code % 7;
    bool empty = false;
    int i = 0;
    while (!empty)
    {
        hash += (i * (code % 5 + 1));
        if (table[hash % size].author_key.AuthorName == "" || table[hash % size].author_key.AuthorName == Name)
        {
            hash %= size;
            empty = true;
        }
        else
        {
            hash = code % 7;
        }
        i++;
    }
    return hash;
}
int hashtable::hash_code(string &Name)
{
    int length = Name.length();
    int code = 0;
    for (int i = 0; i < length; i++)
    {
        if (Name[i] == ' ')
        {
            code += (27 * pow(27, length - i - 1));
        }
        else
        {
            code += (tolower(Name[i]) - 'a' + 1) * pow(27, (length - i - 1));
        }
    }
    return code;
}
bool hashtable::exist(string Name)
{
    int code = this->hash_code(Name);
    int hash =code % 7;
    int i=0;
    while (table[hash % size].author_key.AuthorName != "")
    {
        if (table[hash % size].author_key.AuthorName == Name)
        {
            return true;
        }
        if (i == size)
        {
            break;
        }
        i++;
        hash += (code % 5 + 1);
    }
    return false;
}

void hashtable::insert(AuthorKey &key, AuthorValue &value)
{
    if (exist(key.AuthorName))
    {
        AuthorAndIndex a = findAllBooks(key.AuthorName);
        table[a.index].author_value.Bookid.push_back(value.Bookid.front());
    }
    else
    {
        int hash = gethash(key.AuthorName);
        loadness++;
        table[hash].author_key.AuthorName = key.AuthorName;
        table[hash].author_value.Bookid.push_back(value.Bookid.front());
    }
}
void hashtable::print()
{
    cout << endl;
    cout << " Second Hashtable now looks like: " << endl;
    cout << endl;
    for (int i = 0; i < size; i++)
    {
        if (table.at(i).author_key.AuthorName != "")
        {
            cout << endl;
            cout << "Slot number " << i << endl << "Name:" << table.at(i).author_key.AuthorName << endl;
            cout << "BookID: " << endl;
            for (auto item = table.at(i).author_value.Bookid.begin(); item != table.at(i).author_value.Bookid.end(); ++item)
            {
                cout << (*item) << " | ";
            }
            cout << endl;
        }
    }
}
AuthorAndIndex hashtable::findAllBooks(string &name)
{
    int code = this->hash_code(name);
    int hash =code % 7;
    while (table[hash % size].author_key.AuthorName != name)
    {
        hash += (code % 5 + 1);
    }
    AuthorValue v;
    v = table[hash % size].author_value;
    AuthorAndIndex a{v, hash % size};
    return a;
}