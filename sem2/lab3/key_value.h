#pragma once
#include <iostream>
#include <list>
using namespace std;

struct AuthorsList 
{
    list<string> AuthorsName;
};

struct BookKey 
{
    int bookID;
};
 
struct BookValue 
{
    string title; 
    AuthorsList authors;
    int yearofPublishing;
};
struct BookPair
{
    BookKey book_key;
    BookValue book_value;
};
struct AuthorKey
{
    string AuthorName;
};
struct AuthorValue
{
    list<int> Bookid;
};
struct AuthorPair
{
    AuthorKey author_key;
    AuthorValue author_value;
};
struct AuthorAndIndex
{
    AuthorValue author_value;
    int index;
};
