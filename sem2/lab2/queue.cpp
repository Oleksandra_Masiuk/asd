#include "queue.h"
using namespace std;
Queue::Queue()
{
    size_ = 6;
    array = new string[size_];
    head = -1;
    tail = -1;
}
Queue::~Queue()
{
    delete[] array;
    size_ = 0;
    head = -1;
    tail = -1;
}
void Queue::resize()
{
    string * array2;
    array2 =new string[size_ * 2];
    if (head <= tail)
    {
        for (int i = 0; i <= tail; i++)
        {
            array2[i] = array[i];
        }
    }
    else
    {
        for (int i = 0; i <= tail; i++)
        {
            array2[i] = array[i];
        }
        for (int i = tail + 1; i < size_; i++)
        {
            array2[i + size_] = array[i];
        }
        head=head+size_;
    }
    size_ = size_ * 2;
    delete[] array;
    array = array2;
}
void Queue::enqueue(string &str)
{
    if ((tail + 1) % size_ == head)
    {
        resize();
        cerr << "queue is full, try again later";
    }
    else
    {
        if (head == -1)
        {
            head = 0;
        }
        tail = (tail + 1) % size_;
        array[tail] = str;
    }
}
void Queue::dequeue()
{
    if (head == -1 && tail == -1)
    {
        cout << "queue is empty  add something and try again";
    }
    else
    {
        if (tail == head)
        {
            cout << "now queue is empty " << endl;
            head = tail = -1;
        }
        else
        {
            head = (head + 1) % size_;
        }
    }
}
void Queue::print()
{
    cout << endl
         << "Queue:" << endl;
    if (head == -1 && tail == -1)
    {
        cout << "queue is empty  add something and try again";
    }
    else
    {
        int i;
        if (head <= tail)
        {
            for (i = head; i <= tail; i++)
            {
                cout << array[i] << endl;
            }
        }
        else
        {
            i = head;
            while (i < size_)
            {
                cout << array[i] << endl;
                i++;
            }
            i = 0;
            while (i <= tail)
            {
                cout << array[i] << endl;
                i++;
            }
        }
    }
}