#include <cstring>
#include <iostream>
using namespace std;
struct Queue
{
    string * array;
    int head;
    int tail;
    size_t size_;
    
    Queue();
    ~Queue();
    void enqueue( string & str);
    void dequeue();
    void resize();
    void print();
};